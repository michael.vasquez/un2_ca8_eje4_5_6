# Escribe un programa para abrir el archivo y leerlo linea por linea
# En cada linea dividir la linea en una lista de palabras
# Revisar si cada palabra se encuentra en la lista
# Ordenar e imprimir las palabras

import os

archivo = input("Ingresa un nombre del archivo: ")
if archivo == 'romeo.txt':
    a = open('romeo.txt')
    lista = []
    for p in a:
        palabra = p.rstrip().split()
        for elemento in palabra:
            if elemento in lista:
                continue
            else:
                lista.append(elemento)
    lista.sort()
    print(lista)
else:
    print("El archivo es incorrecto")
