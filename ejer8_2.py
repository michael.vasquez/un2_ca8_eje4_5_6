# Escribe un programa para leer a travez de datos de una bandeja de entrada de correo
# Identifique en las lineas la palabra "from" imprima lo que va a continuacion
# Imprimir el total final

archivo = input("Ingresa el nombre del archivo: ")

if archivo == 'mbox-short.txt':
    a = open("mbox-short.txt")
    count = 0
    for line in a:
        line = line.rstrip()
        if line == "": continue
        words = line.split()
        if words[0] != "From": continue

        print(count, words[1])
        count = count + 1
    print("En el documento hay ", count, "correos")
else:
    print("El nombre es incorrecto")