# Pedir al usuario una lista de numeros
# Imprimir el mayo y menor de dicha lista
# Cuando el usuario termine escriba "hecho", para terminar
cont = 0
total = 0
lista = []

while True:
    valor = input("Introduce un numero (o 'hecho' para terminar): ")
    lista.append(valor)
    if valor.lower() in "hecho":
        break
    try:
        total += float(valor)
        cont += 1
        mayor = max(lista)
        menor = min(lista)
    except ValueError:
        print("ERROR, no es un numero\nIntentalo de nuevo...")

print("El numero mayor ingresado es: ", float(mayor))
print("El numero menor ingresado es: ", float(menor))
